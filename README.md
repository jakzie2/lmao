# LMAO - Lightweight Mod and Addon Organizer

LMAO is a simple open-source mod manager for both Windows and Linux made mainly for games which doesn't have their own. It allows you to create separate profiles with each having its own mods and saves, while being able to freely switch between them with just one click.

More info: [http://jakzie2.8u.cz/lmao](http://jakzie2.8u.cz/lmao)

## Installation

The simplest way to get the program running is to download an executable from the link above.

If you don't trust the executables and want to run the program from source anyway, follow these steps:

1. Make sure you have Python 3.8 or newer (may work on older versions, but I won't do any testing on them, so no promises)

2. Clone the repository:

```
git clone https://gitlab.com/jakzie2/lmao
```

3. Install `pyyaml` and `tk` modules:

```
pip install pyyaml tk
```

4. Now you should be able to launch the program with:

```
python main.py
```

5. If you want to create an executable for the program, you can do it using the `pyinstaller` module:

```
pip install pyinstaller
pyinstaller main.py -F -n LMAO --noconsole
```

6. Now you should find the executable in a newly created `dist` folder.

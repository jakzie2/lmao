import os
import tkinter as tk
from tkinter import messagebox
from tkinter import filedialog

import lmao
import server


class GUILogger:
    def log(self, msg):
        print(msg)

    def info(self, msg):
        messagebox.showinfo(message=msg)
        print(msg)

    def error(self, msg):
        messagebox.showerror(message=msg)
        print(msg)

    def confirm(self, msg):
        return messagebox.askyesno(message=msg)


class ChildWindow:
    def __init__(self, parent, title):
        self.parent = parent
        self.window = tk.Toplevel(parent.window)
        self.window.title(title)
        self.draw()
        pos_x = (self.window.winfo_screenwidth() / 2) - (self.window.winfo_reqwidth() / 2)
        pos_y = (self.window.winfo_screenheight() / 2) - (self.window.winfo_reqheight() / 2)
        self.window.geometry('+%d+%d' % (pos_x, pos_y))
        self.window.wait_visibility()
        self.window.grab_set()
        self.window.protocol('WM_DELETE_WINDOW', self.close)

    def close(self):
        if self.on_close():
            self.window.destroy()

    def on_close(self):
        return True

    def draw(self):
        pass

    def add_basic_buttons(self, frame, confirm_text, cancel_text, confirm_handle):
        add_button = tk.Button(frame, text=confirm_text, width=8, height=2)
        add_button.bind('<Button-1>', confirm_handle)
        add_button.grid(column=0, row=0, padx=5, pady=5)

        self.add_close_button(frame, cancel_text, column=1, row=0)

    def add_close_button(self, frame, text, column, row, width=8):
        close_button = tk.Button(frame, text=text, width=width, height=2)
        close_button.bind('<Button-1>', lambda event: self.close())
        close_button.grid(column=column, row=row, padx=5, pady=5)


class AddProfileWindow(ChildWindow):
    def __init__(self, parent, duplicate=False):
        self.name_entry = None
        self.duplicate = duplicate
        super().__init__(parent, 'Clone profile' if duplicate else 'Add profile')

    def add_click(self, event):
        profile_id = self.name_entry.get()
        self.close()
        if self.duplicate:
            success = lmao.duplicate_profile(self.parent.game_id, self.parent.profile_id, profile_id)
        else:
            success = lmao.add_profile(self.parent.game_id, profile_id)

        if success:
            self.parent.draw_profile_window(profile_id)

    def draw(self):
        form_frame = tk.Frame(self.window)
        form_frame.grid(column=0, row=0, padx=10, pady=10)

        name_label = tk.Label(form_frame, text='New name:')
        name_label.grid(column=0, row=0)

        self.name_entry = tk.Entry(form_frame)
        self.name_entry.grid(column=1, row=0)

        buttons_frame = tk.Frame(self.window)
        buttons_frame.grid(column=0, row=1)
        self.add_basic_buttons(buttons_frame, 'Clone' if self.duplicate else 'Add', 'Cancel', self.add_click)


class ManageModsWindow(ChildWindow):
    def __init__(self, parent):
        self.all_mods = None
        self.mods_list = None
        super().__init__(parent, 'Manage mods')

    def ok_click(self, event):
        cs = self.mods_list.curselection()
        self.close()
        lmao.set_profile_mods(self.parent.game_id, self.parent.profile_id, [self.all_mods[i] for i in cs])
        self.parent.draw_profile_window(self.parent.profile_id)

    def draw(self):
        info_label = tk.Label(self.window, text='Select mods for the profile ' + self.parent.profile_id + ':')
        info_label.grid(column=0, row=0, padx=5, pady=5)

        self.all_mods = lmao.fetch_all_mods(self.parent.game_id)
        profile_mods = lmao.get_game_config(self.parent.game_id)['profiles'][self.parent.profile_id]['mods']
        all_mod_names = [(lmao.get_mod_name(self.parent.game_id, mod) or mod) for mod in self.all_mods]
        self.mods_list = tk.Listbox(self.window, height=20, selectmode='multiple', listvariable=tk.StringVar(value=all_mod_names))
        for i, mod in enumerate(self.all_mods):
            if mod in profile_mods:
                self.mods_list.select_set(i)
        self.mods_list.grid(column=0, row=1)

        buttons_frame = tk.Frame(self.window)
        buttons_frame.grid(column=0, row=2)
        self.add_basic_buttons(buttons_frame, 'OK', 'Cancel', self.ok_click)


class ServerWindow(ChildWindow):
    def __init__(self, parent):
        super().__init__(parent, 'Local server')
        self.server = server.Server()

    def on_close(self):
        self.server.stop()
        return True

    def draw(self):
        info_label = tk.Label(self.window, text='Local server is now running...\nExternal apps can connect to LMAO.')
        info_label.grid(column=0, row=0, padx=20, pady=20)

        self.add_close_button(self.window, 'Stop local server', column=0, row=1, width=15)


class ToolsWindow(ChildWindow):
    def __init__(self, parent):
        super().__init__(parent, 'Tools')

    def draw(self):
        tools = lmao.get_game_tools(self.parent.game_id)

        if tools is not None and hasattr(tools, 'get_tools'):
            def get_tool_event(tool_func):
                return lambda event: tool_func()
            for i, tool in enumerate(tools.get_tools()):
                tool_button = tk.Button(self.window, text=tool[0], width=30, height=2)
                tool_button.bind('<Button-1>', get_tool_event(tool[1]))
                tool_button.grid(column=0, row=i, padx=10, pady=10)
        else:
            info_label = tk.Label(self.window, text='This game does not have any tools available.')
            info_label.grid(column=0, row=0, padx=20, pady=20)


class MainWindow:
    def __init__(self):
        lmao.logger = GUILogger()
        self.game_id = None
        self.game_list = None
        self.profile_id = None
        self.layout_frame = None
        self.profile_frame = None
        self.profile_info_frame = None
        self.profile_list = None
        self.details_frame = None
        self.window = tk.Tk()
        self.window.title('LMAO - Lightweight Mod and Addon Organizer (Version 0.1.3, by Jakzie)')
        size_x = 1000
        size_y = 550
        pos_x = (self.window.winfo_screenwidth() / 2) - (size_x / 2)
        pos_y = (self.window.winfo_screenheight() / 2) - (size_y / 2)
        self.window.geometry('%dx%d+%d+%d' % (size_x, size_y, pos_x, pos_y))
        self.draw_main_window()
        self.window.mainloop()

    def add_game_click(self, event):
        def_file = filedialog.askopenfilename()

        if isinstance(def_file, str) and def_file != '':
            if lmao.add_game(def_file):
                self.draw_main_window()

    def remove_game_click(self, event):
        if self.game_id is None:
            messagebox.showerror(message='No game selected')
        elif lmao.remove_game(self.game_id):
            self.game_id = None
            self.profile_id = None
            self.draw_main_window()

    def remove_profile_click(self, event):
        if lmao.remove_profile(self.game_id, self.profile_id):
            self.draw_profile_window()

    def switch_profile_click(self, event):
        lmao.switch_profile(self.game_id, self.profile_id)
        self.draw_profile_window()

    def mod_folder_click(self, event):
        path = lmao.get_game_config(self.game_id)['modFolder']
        if os.name == 'nt':
            os.startfile(path)
        elif os.name == 'posix':
            os.system('xdg-open "' + path + '"')

    def game_list_click(self, event):
        cs = event.widget.curselection()
        if len(cs) > 0:
            self.game_id = self.game_list.get(cs[0])
            lmao.sync_active_mods(self.game_id)
            self.draw_profile_window()

    def profile_list_click(self, event):
        cs = event.widget.curselection()
        if len(cs) > 0:
            self.profile_id = self.profile_list.get(cs[0])
            self.draw_details_window()

    def draw_main_window(self):
        self.layout_frame = tk.Frame(self.window)
        self.layout_frame.grid(column=0, row=0)

        game_frame = tk.Frame(self.layout_frame)
        game_frame.grid(column=0, row=0, padx=10, pady=10, sticky="N")

        main_button_frame = tk.Frame(game_frame)
        main_button_frame.grid(column=0, row=0)

        add_game_button = tk.Button(main_button_frame, text='Add game', width=12, height=2)
        add_game_button.bind('<Button-1>', self.add_game_click)
        add_game_button.grid(column=0, row=0, padx=5, pady=5)

        remove_game_button = tk.Button(main_button_frame, text='Remove game', width=12, height=2)
        remove_game_button.bind('<Button-1>', self.remove_game_click)
        remove_game_button.grid(column=1, row=0, padx=5, pady=5)

        self.game_list = tk.Listbox(game_frame, height=25, listvariable=tk.StringVar(value=lmao.get_config()['games']))
        self.game_list.bind('<<ListboxSelect>>', self.game_list_click)
        self.game_list.grid(column=0, row=1)

        self.draw_profile_window()

        bottom_frame = tk.Frame(self.layout_frame)
        bottom_frame.grid(column=0, row=1)

        start_server_button = tk.Button(bottom_frame, text='Start local server...', width=20, height=2)
        start_server_button.bind('<Button-1>', lambda event: ServerWindow(self))
        start_server_button.grid(column=0, row=0, padx=5, pady=5)

    def draw_profile_window(self, sel_profile_id=None):
        if self.profile_frame is not None:
            self.profile_frame.destroy()

        if self.game_id is None:
            return

        game_config = lmao.get_game_config(self.game_id)
        profile_keys = list(game_config['profiles'].keys())

        self.profile_id = sel_profile_id or lmao.get_game_cache(self.game_id)['activeProfile']

        self.profile_frame = tk.Frame(self.layout_frame)
        self.profile_frame.grid(column=1, row=0, padx=10, pady=10, sticky="N")

        game_label = tk.Label(self.profile_frame, text=game_config['name'])
        game_label.config(font=(game_label['font'][0], 20))
        game_label.grid(column=0, row=0, padx=10, pady=10, sticky="W")

        profile_button_frame = tk.Frame(self.profile_frame)
        profile_button_frame.grid(column=0, row=1, sticky="W")

        add_profile_button = tk.Button(profile_button_frame, text='Add profile...', width=12, height=2)
        add_profile_button.bind('<Button-1>', lambda event: AddProfileWindow(self))
        add_profile_button.grid(column=0, row=0, padx=5, pady=5)

        remove_profile_button = tk.Button(profile_button_frame, text='Remove profile', width=12, height=2)
        remove_profile_button.bind('<Button-1>', self.remove_profile_click)
        remove_profile_button.grid(column=1, row=0, padx=5, pady=5)

        duplicate_profile_button = tk.Button(profile_button_frame, text='Clone profile...', width=12, height=2)
        duplicate_profile_button.bind('<Button-1>', lambda event: AddProfileWindow(self, duplicate=True))
        duplicate_profile_button.grid(column=2, row=0, padx=5, pady=5)

        mod_folder_button = tk.Button(profile_button_frame, text='Open mod folder', width=14, height=2)
        mod_folder_button.bind('<Button-1>', self.mod_folder_click)
        mod_folder_button.grid(column=3, row=0, padx=5, pady=5)

        tools_button = tk.Button(profile_button_frame, text='Tools...', width=10, height=2)
        tools_button.bind('<Button-1>', lambda event: ToolsWindow(self))
        tools_button.grid(column=4, row=0, padx=5, pady=5)

        self.profile_info_frame = tk.Frame(self.profile_frame)
        self.profile_info_frame.grid(column=0, row=2, sticky="W")

        profile_select_frame = tk.Frame(self.profile_info_frame)
        profile_select_frame.grid(column=0, row=1, padx=10, pady=9, sticky="N")

        self.profile_list = tk.Listbox(profile_select_frame, height=21, listvariable=tk.StringVar(value=profile_keys))
        self.profile_list.select_set(profile_keys.index(self.profile_id))
        self.profile_list.bind('<<ListboxSelect>>', self.profile_list_click)
        self.profile_list.grid(column=0, row=1)

        self.draw_details_window()

    def draw_details_window(self):
        if self.profile_id is None:
            return

        if self.details_frame is not None:
            self.details_frame.destroy()

        self.details_frame = tk.Frame(self.profile_info_frame)
        self.details_frame.grid(column=1, row=1, padx=10, sticky="N")

        mod_list = [(lmao.get_mod_info_str(self.game_id, mod) or mod) for mod in lmao.get_game_config(self.game_id)['profiles'][self.profile_id]['mods']]
        mod_desc = 'Profile name:\n\n    ' + self.profile_id + '\n\nMods:\n\n    ' + '\n    '.join(mod_list)
        details_name_label = tk.Label(self.details_frame, text=mod_desc, wraplength=500, justify='left')
        details_name_label.grid(column=0, row=1, pady=20, sticky="W")

        reload = lmao.get_game_cache(self.game_id)['activeProfile'] == self.profile_id
        switch_profile_button = tk.Button(self.details_frame, text='Reload this profile' if reload else 'Switch to this profile', width=15, height=2)
        switch_profile_button.grid(column=0, row=2, padx=5, pady=5, sticky="W")
        switch_profile_button.bind('<Button-1>', self.switch_profile_click)

        manage_mods_button = tk.Button(self.details_frame, text='Manage mods...', width=15, height=2)
        manage_mods_button.grid(column=0, row=3, padx=5, pady=5, sticky="W")
        manage_mods_button.bind('<Button-1>', lambda event: ManageModsWindow(self))

import yaml
import os
import shutil
from pathlib import Path
from importlib.machinery import SourceFileLoader

import utils

app_data_folder = None
config = None
game_configs = {}
game_caches = {}
game_tools = {}
mod_infos = {}


class Logger:
    def log(self, msg):
        print(msg)

    def info(self, msg):
        print(msg)

    def error(self, msg):
        print(msg)

    def confirm(self, msg):
        print(msg + ' (y/N)')

        return input().lower() == 'y'


logger = Logger()


def get_app_data():
    global app_data_folder
    if app_data_folder is not None:
        return app_data_folder

    if os.name == 'nt':
        app_data_folder = os.getenv('APPDATA') + '/LMAO/'
    elif os.name == 'posix':
        app_data_folder = os.getenv('HOME') + '/.lmao/'

    Path(app_data_folder).mkdir(parents=True, exist_ok=True)

    return app_data_folder


def get_config():
    global config
    if config is not None:
        return config

    if not os.path.exists(get_app_data() + 'config.yml'):
        with open(get_app_data() + 'config.yml', 'w', encoding='utf-8') as file:
            file.write('games: []')

    with open(get_app_data() + 'config.yml', 'r', encoding='utf-8') as file:
        config = yaml.load(file.read(), yaml.Loader)

    return config


def save_config():
    with open(get_app_data() + 'config.yml', 'w', encoding='utf-8') as file:
        file.write(yaml.dump(config))


def add_game(definition, ask_update=True):
    if isinstance(definition, str):
        try:
            with open(definition, 'r', encoding='utf-8') as file1:
                game_def = yaml.load(file1.read(), yaml.Loader)
        except (ValueError, yaml.YAMLError):
            logger.error('Selected file isn\'t a valid LMAO definition file.')
            return False
    elif isinstance(definition, dict):
        game_def = definition
    else:
        logger.error('Invalid definition.')
        return False

    game_id = game_def['id']

    if game_id in get_config()['games']:
        if not ask_update or logger.confirm('Game ' + game_id + ' is already registered. Do you want to update its configuration from this definition?'):
            game_def['profiles'] = get_game_config(game_id)['profiles']
            game_configs[game_id] = game_def
            save_game_config(game_id)

            logger.info('Configuration of ' + game_id + ' has been successfully updated.')
            return True
        return False

    game_def['profiles'] = {'default': {'mods': []}}

    with open(get_app_data() + game_id + '-config.yml', 'w', encoding='utf-8') as file2:
        file2.write(yaml.dump(game_def))

    game_cache = {'activeProfile': 'default', 'loadedMods': [], 'loadedSecondaryMods': []}

    with open(get_app_data() + game_id + '-cache.yml', 'w', encoding='utf-8') as file3:
        file3.write(yaml.dump(game_cache))

    try:
        os.mkdir(get_app_data() + game_id + '-mods')
        os.mkdir(get_app_data() + game_id + '-saves')
    except FileExistsError:
        pass

    get_config()['games'].append(game_id)
    save_config()

    game_configs[game_id] = game_def
    game_caches[game_id] = game_cache

    logger.info('Game ' + game_id + ' has been successfully registered.')

    return True


def remove_game(game_id):
    if not logger.confirm('Removing a game will delete all the mods and saves of inactive profiles. Do you really want to remove the game ' + game_id + '?'):
        return False

    if game_id not in get_config()['games']:
        logger.error('Game ' + game_id + ' not found.')
        return False

    try:
        os.remove(get_app_data() + game_id + '-config.yml')
        os.remove(get_app_data() + game_id + '-cache.yml')
        shutil.rmtree(get_app_data() + game_id + '-mods')
        shutil.rmtree(get_app_data() + game_id + '-saves')
    except FileNotFoundError:
        pass
    get_config()['games'].remove(game_id)
    save_config()

    game_configs.pop(game_id)
    game_caches.pop(game_id)

    logger.info('Game ' + game_id + ' has been successfully unregistered.')

    return True


def get_game_config(game_id):
    if game_id not in game_configs:
        with open(get_app_data() + game_id + '-config.yml', 'r', encoding='utf-8') as file:
            game_configs[game_id] = yaml.load(file.read(), yaml.Loader)

    return game_configs[game_id]


def save_game_config(game_id):
    with open(get_app_data() + game_id + '-config.yml', 'w', encoding='utf-8') as file:
        file.write(yaml.dump(game_configs[game_id]))


def get_game_cache(game_id):
    if game_id not in game_caches:
        with open(get_app_data() + game_id + '-cache.yml', 'r', encoding='utf-8') as file:
            game_caches[game_id] = yaml.load(file.read(), yaml.Loader)

    return game_caches[game_id]


def save_game_cache(game_id):
    with open(get_app_data() + game_id + '-cache.yml', 'w', encoding='utf-8') as file:
        file.write(yaml.dump(game_caches[game_id]))


def get_game_tools(game_id):
    if game_id not in game_tools:
        if "linkedExtension" in get_game_config(game_id):
            tools = load_extension(game_id + "_tools", get_game_config(game_id)['linkedExtension'])
            if tools is None:
                logger.error("Cannot load tools for the game " + game_id)
                game_tools[game_id] = None
            else:
                game_tools[game_id] = tools
        else:
            game_tools[game_id] = None

    return game_tools[game_id]


def switch_profile(game_id, profile_id):
    game_config = get_game_config(game_id)
    mod_folder = utils.path(game_config['modFolder'])
    save_folders = utils.paths(game_config['saveFolders'])
    secondary_mod_source = utils.path(game_config['secondaryModSource'])

    old_cache = get_game_cache(game_id)
    prev_profile_id = old_cache['activeProfile']

    if 'loadedMods' in old_cache:
        for mod in old_cache['loadedMods']:
            try:
                shutil.copytree(mod_folder + mod, get_app_data() + game_id + '-mods/' + mod)
            except FileExistsError:
                shutil.rmtree(get_app_data() + game_id + '-mods/' + mod, onerror=utils.on_rm_error)
                shutil.copytree(mod_folder + mod, get_app_data() + game_id + '-mods/' + mod)
            except FileNotFoundError:
                logger.log('Mod ' + mod + ' not found and skipped.')
                continue
            shutil.rmtree(mod_folder + mod, onerror=utils.on_rm_error)
            logger.log('Folder removed: ' + mod_folder + mod)
    if 'loadedSecondaryMods' in old_cache:
        for mod in old_cache['loadedSecondaryMods']:
            try:
                shutil.rmtree(mod_folder + mod, onerror=utils.on_rm_error)
            except FileNotFoundError:
                logger.log('Mod ' + mod + ' not found and skipped.')
                continue
            logger.log('Folder removed: ' + mod_folder + mod)

    logger.log('All mods removed successfully.')

    try:
        shutil.copytree(save_folders[0], get_app_data() + game_id + '-saves/' + prev_profile_id)
    except FileExistsError:
        shutil.rmtree(get_app_data() + game_id + '-saves/' + prev_profile_id)
        shutil.copytree(save_folders[0], get_app_data() + game_id + '-saves/' + prev_profile_id)
    except FileNotFoundError:
        logger.log('Save folder not found')
        os.mkdir(get_app_data() + game_id + '-saves/' + prev_profile_id)

    for save_folder in save_folders:
        try:
            shutil.rmtree(save_folder)
        except FileNotFoundError:
            pass

    logger.log('All saves removed successfully.')
    logger.log('Loading profile (' + profile_id + ')...')

    if profile_id not in game_config['profiles']:
        logger.log('This profile does not exist. Switching to default profile.')
        profile_id = 'default'

    cache = {'activeProfile': profile_id, 'loadedMods': [], 'loadedSecondaryMods': []}

    mods_to_load = []
    mods_to_load.extend(game_config['profiles'][profile_id]['mods'])
    mods_to_load.extend(game_config['globalMods'])
    for mod in mods_to_load:
        path_from = get_app_data() + game_id + '-mods/' + mod

        secondary = False
        if not os.path.exists(path_from):
            path_from = secondary_mod_source + mod
            if not os.path.exists(path_from):
                logger.log('Mod ' + mod + ' not found. Skipped.')
                continue
            secondary = True

        try:
            shutil.copytree(path_from, mod_folder + mod)
        except FileExistsError:
            logger.log('Mod ' + mod + ' is already loaded. Skipped.')
            continue

        if secondary:
            cache['loadedSecondaryMods'].append(mod)
        else:
            shutil.rmtree(path_from, onerror=utils.on_rm_error)
            cache['loadedMods'].append(mod)

        logger.log('Folder moved: ' + path_from)
        logger.log('Mod ' + mod + ' loaded successfully.')
    logger.log('All mods loaded. Creating cache...')

    path_from = get_app_data() + game_id + '-saves/' + profile_id
    for save_folder in save_folders:
        try:
            shutil.copytree(path_from, save_folder)
        except FileNotFoundError:
            logger.log('No saves found.')
            os.mkdir(save_folder)
    try:
        shutil.rmtree(path_from)
    except FileNotFoundError:
        pass

    logger.log('All saves loaded.')

    game_caches[game_id] = cache
    save_game_cache(game_id)

    if profile_id == prev_profile_id:
        logger.info('Profile successfully reloaded.')
    else:
        logger.info('Profile successfully switched.')


def add_profile(game_id, profile_id):
    if profile_id in get_game_config(game_id)['profiles']:
        logger.error('Profile ' + profile_id + ' already exists.')
        return False

    if len(profile_id) < 3 or len(profile_id) > 30:
        logger.error('Profile name has to be between 3-30 characters long.')
        return False

    try:
        os.mkdir(get_app_data() + game_id + '-saves/' + profile_id)
    except FileExistsError:
        pass
    except OSError:
        logger.error('Profile name contains invalid characters.')
        return False

    get_game_config(game_id)['profiles'][profile_id] = {'mods': []}
    save_game_config(game_id)
    logger.info('Profile ' + profile_id + ' has been added.')

    return True


def duplicate_profile(game_id, old_profile_id, new_profile_id):
    if not add_profile(game_id, new_profile_id):
        return False

    if get_game_cache(game_id)['activeProfile'] == old_profile_id:
        save_folder = utils.paths(get_game_config(game_id)['saveFolders'])[0]
    else:
        save_folder = get_app_data() + game_id + '-saves/' + old_profile_id

    try:
        shutil.copytree(save_folder, get_app_data() + game_id + '-saves/' + new_profile_id, dirs_exist_ok=True)
    except FileNotFoundError:
        logger.log('Save folder not found')

    get_game_config(game_id)['profiles'][new_profile_id]['mods'].extend(get_game_config(game_id)['profiles'][old_profile_id]['mods'])
    save_game_config(game_id)

    logger.info('Saves and mods have been copied from profile ' + old_profile_id + ' to profile ' + new_profile_id)

    return True


def remove_profile(game_id, profile_id):
    if profile_id == 'default':
        logger.error('Default profile cannot be removed')
        return False
    elif get_game_cache(game_id)['activeProfile'] == profile_id:
        logger.error('Active profile cannot be removed')
        return False
    elif not logger.confirm('Removing a profile will delete all its saves. Do you really want to remove the profile ' + profile_id + '?'):
        return False

    if profile_id not in get_game_config(game_id)['profiles']:
        logger.error('Profile ' + profile_id + ' not found.')
        return False

    try:
        shutil.rmtree(get_app_data() + game_id + '-saves/' + profile_id)
    except FileNotFoundError:
        pass
    get_game_config(game_id)['profiles'].pop(profile_id)
    save_game_config(game_id)
    logger.info('Profile ' + profile_id + ' has been removed.')

    return True


def fetch_all_mods(game_id):
    mods = []
    mods.extend(get_game_cache(game_id)['loadedMods'])
    try:
        mods.extend(utils.list_folders(get_app_data() + game_id + '-mods/'))
    except FileNotFoundError:
        os.mkdir(get_app_data() + game_id + '-mods/')
    try:
        mods.extend(utils.list_folders(get_game_config(game_id)['secondaryModSource']))
    except FileNotFoundError:
        logger.log('Secondary mod source not found.')

    for mod in get_game_config(game_id)['ignoredMods']:
        if mod in mods:
            mods.remove(mod)
    for mod in get_game_config(game_id)['globalMods']:
        if mod in mods:
            mods.remove(mod)

    return mods


def set_profile_mods(game_id, profile_id, mods):
    get_game_config(game_id)['profiles'][profile_id]['mods'] = mods
    save_game_config(game_id)
    if get_game_cache(game_id)['activeProfile'] == profile_id:
        logger.info('Mod list for profile ' + profile_id + ' changed. Because this profile is currently active, you have to reload it to apply changes.')
    else:
        logger.info('Mod list for profile ' + profile_id + ' changed.')


def sync_active_mods(game_id):
    profile_id = get_game_cache(game_id)['activeProfile']
    active_mods = utils.list_folders(get_game_config(game_id)['modFolder'])
    ignored_mods = get_game_config(game_id)['ignoredMods']
    global_mods = get_game_config(game_id)['globalMods']
    for mod in ignored_mods:
        if mod in active_mods:
            active_mods.remove(mod)
    loaded_mods = get_game_cache(game_id)['loadedMods']
    loaded_secondary_mods = get_game_cache(game_id)['loadedSecondaryMods']
    profile_mods = get_game_config(game_id)['profiles'][profile_id]['mods']

    for mod in loaded_mods:
        if mod not in active_mods:
            loaded_mods.remove(mod)
            if mod in profile_mods:
                profile_mods.remove(mod)
            logger.info('Mod ' + mod + ' removed from profile ' + profile_id)

    for mod in loaded_secondary_mods:
        if mod not in active_mods:
            loaded_secondary_mods.remove(mod)
            if mod in profile_mods:
                profile_mods.remove(mod)
            logger.info('Mod ' + mod + ' removed from profile ' + profile_id)

    for mod in active_mods:
        if mod.isdigit():
            if mod not in loaded_secondary_mods:
                loaded_secondary_mods.append(mod)
                if mod not in profile_mods and mod not in global_mods:
                    profile_mods.append(mod)
                logger.info('Mod ' + mod + ' added to profile ' + profile_id)
        elif mod not in loaded_mods:
            loaded_mods.append(mod)
            if mod not in profile_mods and mod not in global_mods:
                profile_mods.append(mod)
            logger.info('Mod ' + mod + ' added to profile ' + profile_id)

    save_game_config(game_id)
    save_game_cache(game_id)

    logger.log('Mods of profile ' + profile_id + ' were synchronized.')


def rename_profile(game_id, profile_id, new_profile_id):
    get_game_config(game_id)['profiles'][new_profile_id] = get_game_config(game_id)['profiles'][profile_id]
    get_game_config(game_id)['profiles'].pop(profile_id)
    save_game_config(game_id)

    if get_game_cache(game_id)['activeProfile'] == profile_id:
        get_game_cache(game_id)['activeProfile'] = new_profile_id
        save_game_cache(game_id)
    else:
        try:
            os.rename(get_app_data() + game_id + '-saves/' + profile_id, get_app_data() + game_id + '-saves/' + new_profile_id)
        except FileNotFoundError:
            os.mkdir(get_app_data() + game_id + '-saves/' + new_profile_id)


def get_mod_name(game_id, mod):
    mod_info = get_mod_info(game_id, mod)
    if mod_info is None:
        return None
    else:
        return mod_info['name']


def get_mod_info(game_id, mod):
    if game_id not in mod_infos:
        mod_infos[game_id] = {}

    if mod in mod_infos[game_id]:
        return mod_infos[game_id][mod]

    tools = get_game_tools(game_id)

    if not hasattr(tools, "fetch_mod_info"):
        return None

    if mod in get_game_cache(game_id)['loadedMods']:
        mod_path = utils.path(get_game_config(game_id)['modFolder']) + mod
    elif mod.isdigit():
        mod_path = utils.path(get_game_config(game_id)['secondaryModSource']) + mod
    else:
        mod_path = get_app_data() + game_id + '-mods/' + mod

    if not os.path.exists(mod_path):
        return None

    mod_info = tools.fetch_mod_info(mod_path)

    if mod_info is None or 'name' not in mod_info:
        return None
    else:
        mod_infos[game_id][mod] = mod_info
        return mod_info


def get_mod_info_str(game_id, mod):
    mod_info = get_mod_info(game_id, mod)
    if mod_info is None:
        return None
    mod_info_str = mod_info['name']
    if 'version' in mod_info:
        mod_info_str += ' (' + mod_info['version'] + ')'
    if 'author' in mod_info:
        mod_info_str += ' by ' + mod_info['author']

    return mod_info_str


def load_extension(name, path):
    try:
        ext_module = SourceFileLoader(name, path).load_module()
    except ImportError:
        return None

    if hasattr(ext_module, "LMAOExtension"):
        return ext_module.LMAOExtension()

    return None


def call_extension(name, path, method, params):
    ext = load_extension(name, path)

    if ext is None:
        logger.error("Cannot load LMAO extension " + name + " with path: " + path)
        return

    if hasattr(ext, "call"):
        if not ext.call(path, method, params):
            logger.error("Extension " + name + " called with invalid parameters.")
    else:
        logger.error("Extension " + name + " has no call function.")

import sys
import lmao
import gui

if __name__ == '__main__':
    args = sys.argv
    if len(args) == 1:
        gui.MainWindow()
    elif len(args) >= 3 and args[1] == 'add_game':
        lmao.add_game(' '.join(args[2:]))
    elif len(args) == 3 and args[1] == 'remove_game':
        lmao.remove_game(args[2])
    elif len(args) >= 4 and args[1] == 'switch_profile':
        lmao.switch_profile(args[2], ' '.join(args[3:]))
    elif len(args) >= 4 and args[1] == 'add_profile':
        lmao.add_profile(args[2], ' '.join(args[3:]))
    elif len(args) >= 4 and args[1] == 'remove_profile':
        lmao.remove_profile(args[2], ' '.join(args[3:]))
    elif len(args) == 3 and args[1] == 'fetch_all_mods':
        print(lmao.fetch_all_mods(args[2]))
    elif len(args) >= 5 and args[1] == 'set_profile_mods':
        lmao.set_profile_mods(args[2], args[3], args[4:])
    elif len(args) >= 3 and args[1] == 'sync_active_mods':
        lmao.sync_active_mods(args[2])
    elif len(args) == 4 and args[1] == 'get_mod_name':
        print(lmao.get_mod_name(args[2], args[3]))
    else:
        print('Invalid command')

import threading
import json
from http.server import BaseHTTPRequestHandler, HTTPServer
from http.client import HTTPConnection

import lmao

events = []


class Handler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.end_headers()

    def do_POST(self):
        post_data = self.rfile.read(int(self.headers['Content-Length']))
        content_str = post_data.decode('utf-8')
        try:
            content = json.loads(content_str)
        except json.decoder.JSONDecodeError:
            self.send_error_response()
            return

        for k, v in content.items():
            events.append((k, v))

        self.send_response(200)
        self.end_headers()
        message = "lmao-success"
        self.wfile.write(bytes(message, "utf8"))

    def send_error_response(self):
        self.send_response(400)
        self.end_headers()


class Server:
    def __init__(self):
        def run_server():
            while self.running:
                self.server.handle_request()
                while len(events) > 0:
                    k, v = events[0]
                    if k == "lmao-call-extension":
                        lmao.call_extension(v["name"], v["path"], v["method"], v["params"])
                    events.remove(events[0])

        self.server = HTTPServer(("localhost", 8040), Handler)
        self.thread = threading.Thread(target=run_server)
        self.thread.daemon = True
        self.running = True
        self.thread.start()
        lmao.logger.log('Local server started at port 8040')

    def stop(self):
        self.running = False
        con = HTTPConnection("localhost", 8040)
        con.request("GET", "/")
        con.close()
        self.thread.join()
        self.server.server_close()
        lmao.logger.log('Local server stopped')

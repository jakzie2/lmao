import os
import stat


def path(string):
    if string[-1] == '/' or string[-1] == '\\':
        return string

    return string + '/'


def paths(array):
    new_array = []
    for string in array:
        new_array.append(path(string))

    return new_array


def on_rm_error(func, pth, exc_info):
    os.chmod(pth, stat.S_IWRITE)
    os.unlink(pth)


def list_folders(pth):
    return [f.name for f in os.scandir(path(pth)) if f.is_dir()]
